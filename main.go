package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/zcong1993/token/common"
	"github.com/zcong1993/token/store"
	"net/http"
	"strings"
	"time"
)

func mustUUID() string {
	uu, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}
	return uu.String()
}

var tokenStore = store.NewTokenStore(time.Minute)
var CTX_KEY = "user"

func auth(c *gin.Context) {
	errResp := common.ErrResp{Error: "TOKEN_ERROR"}
	a := c.GetHeader("Authorization")
	token := strings.Replace(a, "Bearer ", "", 1)
	if token == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errResp)
		return
	}

	u := tokenStore.Get(token)

	fmt.Printf("%+v\n", u)

	if u == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errResp)
		return
	}

	c.Set(CTX_KEY, u)
}

func main() {
	s := store.NewStore()

	r := gin.Default()

	r.POST("/login", func(c *gin.Context) {
		var f store.User
		if c.ShouldBind(&f) != nil {
			c.JSON(http.StatusBadRequest, common.ErrResp{Error: "invalid params"})
			return
		}
		ok, u := s.Check(f)

		if ok {
			uu := mustUUID()
			tokenStore.Add(uu, *u)
			c.JSON(http.StatusOK, gin.H{"token": uu})
			return
		}

		c.JSON(http.StatusUnauthorized, common.ErrResp{Error: "password or name error"})
	})

	r.POST("/register", func(c *gin.Context) {
		var f store.User
		if err := c.ShouldBind(&f); err != nil {
			fmt.Printf("register err %+v\n", err)
			c.JSON(http.StatusBadRequest, common.ErrResp{Error: "invalid params"})
			return
		}

		err := s.Save(f)

		if err != nil {
			c.JSON(http.StatusBadRequest, common.ErrResp{Error: err.Error()})
			return
		}

		f.Password = ""

		c.JSON(http.StatusOK, f)
	})

	r.GET("/me", auth, func(c *gin.Context) {
		u, ok := c.Get(CTX_KEY)
		if !ok {
			panic("err")
		}

		c.JSON(http.StatusOK, store.UserShow(*u.(*store.User)))
	})

	r.Run()
}
