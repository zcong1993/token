package store

import (
	"github.com/zcong1993/cache/expire/iexpire"
	"time"
)

type TokenStore struct {
	store      *iexpire.ExipreMap
	D          time.Duration
	gcInterval time.Duration
}

func NewTokenStore(d time.Duration) *TokenStore {
	s := iexpire.NewExpireMap(time.Minute)
	return &TokenStore{
		store:      s,
		D:          d,
		gcInterval: time.Minute,
	}
}

func (tk *TokenStore) Add(key string, u User) {
	tk.store.Set(key, u, tk.D)
}

func (tk *TokenStore) Get(key string) *User {
	v, ok := tk.store.Get(key).(User)
	if !ok {
		return nil
	}
	return &v
}
