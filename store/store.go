package store

import "errors"

type User struct {
	Index    int    `json:"index"`
	Name     string `json:"name" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type UserShow struct {
	Index    int    `json:"index"`
	Name     string `json:"name"`
	Password string `json:"-"`
}

type Store struct {
	index int
	store map[int]User
}

func NewStore() *Store {
	return &Store{
		index: 0,
		store: make(map[int]User, 0),
	}
}

var (
	ErrDumpUser = errors.New("dump user")
)

func (s *Store) Has(name string) bool {
	for _, u := range s.store {
		if u.Name == name {
			return true
		}
	}

	return false
}

func (s *Store) Save(u User) error {
	if s.Has(u.Name) {
		return ErrDumpUser
	}
	s.index++
	u.Index = s.index

	s.store[s.index] = u
	return nil
}

func (s *Store) FindByName(name string) *User {
	for _, u := range s.store {
		if u.Name == name {
			return &u
		}
	}

	return nil
}

func (s *Store) Check(u User) (bool, *User) {
	uu := s.FindByName(u.Name)
	if uu == nil {
		return false, nil
	}

	return uu.Password == u.Password, uu
}
